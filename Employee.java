package bcas.ap.abst;

public class Employee extends Person {
	private int empId;

	public Employee(String nm, String gen,int id) {
		super(nm, gen);
		this.empId=id;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void work() {
		if(empId == 0) {
			System.out.println("Not working");
		}else {
			System.out.println("Working as employee!!");
		}
		
		// TODO Auto-generated method stub
		
	}

public static void main(String args[]) {
	
	Person student = new Employee("Dove", "Female", 0);
	Person employee = new Employee("Sabe","Male",123);
	student.work();
	employee.work();
	
	employee.changeName("Sabashan");
	System.out.println(employee.toString());
}
}
